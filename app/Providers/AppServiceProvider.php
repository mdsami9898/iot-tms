<?php

namespace App\Providers;

use App\Contracts\Repositories\BusRepository;

use App\Contracts\Services\BusContact;
use App\Repositories\BusRepositoryEloquent;

use App\Services\BusService;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->bind(BusContact::class, BusService::class);
        $this->app->bind(BusRepository::class, BusRepositoryEloquent::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Schema::defaultStringLength(191);
    }
}
