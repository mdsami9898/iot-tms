<?php


namespace App\Contracts\Repositories;

use Prettus\Repository\Contracts\RepositoryInterface;

interface BusRepository extends RepositoryInterface{

    public function insertNewRecord(array $data);
}
