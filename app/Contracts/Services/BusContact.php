<?php
namespace App\Contracts\Services;
interface BusContact{
    public function businsert($request);

    public function insertDemoData($request);

    public function getDataByGroupOfBus($request);

    public function getBusNameById($bus_id);

}
