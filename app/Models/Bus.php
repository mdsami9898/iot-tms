<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Bus extends Model
{
    use HasFactory;

    protected $fillable = [
        'user_id',
        'bus_name',
        'total_capacity',
        'bus_color'
    ];
    const Location = [
        'Khilgao',
        'Mugda',
        'Komolapur',
        'Motijhil',
        'Polton',
        'Kolabagan',
        'Dhanmondi 27',
        'Asad Gate',
        'Mirpor 1',
    ];
}
