<?php

namespace App\Services;
use App;


use Illuminate\Contracts\Auth\Guard;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Log;

class BusService implements App\Contracts\Services\BusContact {
    private $busRepository;
    /**
     * @var Guard
     */
    private $auth;
    /**
     * @var App\Repositories\BusDetailsRepositoryEloquent
     */
    private $busDetailsRepository;


    public function __construct(
        App\Repositories\BusRepositoryEloquent $busRepository,
        Guard $auth,
        App\Repositories\BusDetailsRepositoryEloquent $busDetailsRepository
    ){
        $this->busRepository = $busRepository;
        $this->auth = $auth;
        $this->busDetailsRepository = $busDetailsRepository;
    }
    public function businsert($request)
    {
        try
        {
            $name = isset($request['bus_name']) ? $request['bus_name'] : null;
            $totalCapacity = isset($request['total_capacity']) ? $request['total_capacity'] : null;
            if ($name == null){
                return [
                    'html' => 'Bus Name Required',
                    'status' => 'error'
                ];
            }elseif ($totalCapacity == null) {
                return [
                    'html' => 'Total capacity required',
                    'status' => 'error'
                ];
            }
            else{
                $data = [
                    'user_id' => $this->auth->user()->id,
                    'bus_name' => trim($name),
                    'total_capacity' =>(int) $totalCapacity,
                    'bus_color' =>isset($request['bus_color']) ? $request['bus_color'] : null,
                ];

                $saveData = $this->busRepository->insertNewRecord($data);
                if ($saveData){
                    return [
                        'html' => 'Bus Created Successfully',
                        'status' => 'success'
                    ];
                }
            }
        }catch (\Exception $e){
            Log::error("ClientService Error On: ".$e->getLine()."--". $e->getMessage());
            return [
                'html' => 'Sorry Something wrong please try again',
                'status' => 'error'
            ];
        }
    }


    public function insertDemoData($userId)
    {
        $buses = $this->busRepository->getByUserId($userId);

        if (count($buses) > 0){
            foreach ($buses as $key => $value){
                $this->insertBusDetails($value);
            }
        }


        return [
            'html' => 'Random Data Generated Successfully',
            'status' => 'success'
        ];

    }

    private function insertBusDetails($value)
    {

        $this->busDetailsRepository->deleteByWhere(['bus_id'=>$value->bus_id]);
        $finalData = [];
        for ($i = 0; $i<= ($value->total_capacity -1); $i++){
            $createdAt = Carbon::now()->subDays(rand(0, 10));
            $price = rand(10,100);
            $user_id = $value->user_id;
            $bus_id = $value->id;
            $data = [
                'user_id'=>$user_id,
                'bus_id'=>$bus_id,
                'status'=>rand(0,1),
                'in_location'=>null,
                'out_location'=>null,
                'price'=>$price,
                'created_at'=>$createdAt
            ];



            array_push($finalData,$data);
        }
        $result = $this->busDetailsRepository->insertMultiple($finalData);
        if ($result)
            return true;
        return false;



    }



    public function getDataByGroupOfBus($request)
    {

        return $this->busDetailsRepository->getDataByGroupOfBus($request);

    }

    public function getBusNameById($bus_id)
    {
        return $this->busRepository->getNameById($bus_id);
    }
}
