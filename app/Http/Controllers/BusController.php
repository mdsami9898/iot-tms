<?php

namespace App\Http\Controllers;

use App\Contracts\Services\BusContact;
use App\Repositories\BusDetailsRepositoryEloquent;
use App\Repositories\BusRepositoryEloquent;
use Carbon\Carbon;
use Illuminate\Contracts\Auth\Guard;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Response;
use Yajra\DataTables\DataTables;

class BusController extends Controller
{
    private $busService;
    private $busRepository;
    private $auth;
    private $busDetailsRepository;
    public function __construct(
        BusContact $busService,
        BusRepositoryEloquent $busRepository,
        BusDetailsRepositoryEloquent $busDetailsRepository,
        Guard $auth

    ){
        $this->busService = $busService;
        $this->busRepository = $busRepository;
        $this->busDetailsRepository = $busDetailsRepository;
        $this->auth = $auth;
    }
    public function index(){
        return view('user.bus.index');
    }

    public function insertBus(Request $request){

        $response = $this->busService->businsert($request);
        return Response::json([
            'html' => $response['html'],
            'status' => $response['status'],
        ]);
    }

    public function busDataTable(){
        $where = [
            'user_id'=>$this->auth->user()->id,
        ];
        $busList = $this->busRepository->busDataTable($where);
        return Datatables::of($busList)

            ->addColumn('bus_name', function ($busList) {
                $name = $busList->bus_name;
                return $name;
            })
            ->addColumn('total_capacity', function ($busList) {
                $name = $busList->total_capacity;
                return $name;
            })
            ->addColumn('color', function ($busList) {
                $name = $busList->bus_color;
                return $name;
            })
            ->addColumn('action', function ($busList) {
                $name = 'Action';
                return $name;
            })
            ->rawColumns([
                'bus_name',
                'total_capacity',
                'color',
                'action'
            ])
            ->make(true);
    }

    public function dashboardReport(Request $request){

        $dateRange = $this->rearrangeDateRangeCarbon($request->date_range);
        $request['from'] = $dateRange['from'];
        $request['to'] = $dateRange['to'];
        $request['user_id'] =$this->auth->user()->id;

        $result = $this->busService->getDataByGroupOfBus($request)->toArray();

        $data = [];
        $percentData = [];
        $totalPrice = 0;
        if (count($result) > 0){
            foreach ($result as $key => $value){
               $busName = $this->busService->getBusNameById($value['bus_id']);
               $data[$key]['bus_name'] = $busName->bus_name;
               $data[$key]['bus_id'] = $value['bus_id'];
               $data[$key]['total_price'] = $value['total_price'];
                $data[$key]['bus_color'] = isset($busName->bus_color) ? $busName->bus_color : '#ccc';
               $totalPrice =$totalPrice+ $value['total_price'];


            }

            $color = [];
            $name = [];
            $price = [];
            foreach ($result as $key => $value){
                $busName = $this->busService->getBusNameById($value['bus_id']);
                $percentData[$key]['bus_name'] = $busName->bus_name;
                $percentData[$key]['bus_id'] = $value['bus_id'];
                $percentData[$key]['bus_color'] = isset($busName->bus_color) ? $busName->bus_color : '#ccc';
                $percentData[$key]['total_price'] = round(($value['total_price']/$totalPrice)*100);
                array_push($color,isset($busName->bus_color) ? $busName->bus_color : '#ccc');
                array_push($name,$busName->bus_name);
                array_push($price,round(($value['total_price']/$totalPrice)*100));
            }



            return Response::json([
                'color' => $color,
                'name' => $name,
                'price' => $price,
                'status' => 'success',
                'data' => $data,
            ]);


        }



    }

    public static function rearrangeDateRangeCarbon($dateRange){
        $daterange_array = explode('-', $dateRange);
        $daterange_array[0] = trim($daterange_array[0]);
        $daterange_array[1] = trim($daterange_array[1]);

        $date['from'] = Carbon::create($daterange_array[0])->format('Y-m-d');
        $date['to'] = Carbon::create($daterange_array[1])->format('Y-m-d');
        return $date;
    }

    public function demoDataInsert(Request $request){
        $userId = $this->auth->user()->id;
        $response = $this->busService->insertDemoData($userId);
        return Response::json([
            'html' => $response['html'],
            'status' => $response['status'],
        ]);
    }

    public function liveBus(){

        $singleBus= $this->busRepository->getById(1);
        $details = $this->busDetailsRepository->getSingleAmountByBusId(1);
        $rowCount = $this->busDetailsRepository->getRowCount(1)->count();

        return view('user.bus.live-bus')->with([
            'singleBus'=>$singleBus,
            'details'=>$details[0],
            'rowCount'=>$rowCount,
        ]);
    }

    public function liveLocation(){

        return view('user.bus.live-location');
    }
    public function about(){

        return view('user.bus.about');
    }
    public function contact(){

        return view('user.bus.contact');
    }

    public function busDetail($id){

        $singleBus= $this->busRepository->getById($id);
        $details = $this->busDetailsRepository->getSingleAmountByBusId(1);
        $rowCount = $this->busDetailsRepository->getRowCount(1)->count();

        return view('user.bus.bus-details')->with([
            'singleBus'=>$singleBus,
            'details'=>$details[0],
            'rowCount'=>$rowCount,
        ]);
    }

    public function busDynamicData(){
        $rowCount = $this->busDetailsRepository->getRowCount(1)->count();
        return Response::json([
            'html' => $rowCount,
            'status' => 'success'
        ]);
    }
}
