<?php


namespace App\Repositories;


use App\Models\Bus;
use Prettus\Repository\Eloquent\BaseRepository;

class BusRepositoryEloquent extends BaseRepository{

    public function model()
    {
       return Bus::class;
    }

    public function insertNewRecord(array $data)
    {
        return $this->model
            ->insert($data);
    }

    public function busDataTable(array $where)
    {
        return $this->model->where($where);
    }
    public function getByUserId($userId)
    {
        return $this->model->where(['user_id'=>$userId])->get();
    }

    public function getNameById($bus_id)
    {
        return $this->model->select('bus_name', 'bus_color')->where(['id'=>$bus_id])->first();
    }
    public function getById($bus_id)
    {
        return $this->model->where(['id'=>$bus_id])->first();
    }
}
