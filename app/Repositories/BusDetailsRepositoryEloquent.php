<?php


namespace App\Repositories;


use App\Models\Bus;
use App\Models\BusDetails;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;
use Prettus\Repository\Eloquent\BaseRepository;
class BusDetailsRepositoryEloquent extends BaseRepository{

    public function model()
    {
       return BusDetails::class;
    }

    public function deleteByWhere(array $array)
    {
        return $this->model->where($array)->delete();
    }

    public function insertMultiple(array $finalData)
    {
        return $this->model->insert($finalData);
    }

    public function getDataByGroupOfBus($request)
    {
        $where = [
            'user_id'=>$request['user_id']
        ];

        return $this->model
            ->select('bus_id', DB::raw('sum(price) as total_price'))
            ->where($where)
            ->whereDate('created_at', '>=', $request['from'])
            ->whereDate('created_at', '<=', $request['to'])
            ->groupBy('bus_id')
            ->get();

    }
    public function getSingleAmountByBusId($busId)
    {
        $where = [
            'bus_id'=>$busId
        ];

        return $this->model
            ->select('bus_id', DB::raw('sum(price) as total_price'))
            ->where($where)
//            ->whereDate('created_at', '>=',$from)
//            ->whereDate('created_at', '<=', $to)
            ->groupBy('bus_id')
            ->get();

    }
    public function getRowCount($busId)
    {
        $where = [
            'bus_id'=>$busId,
            'status'=>1
        ];

        return $this->model
            ->where($where)
            ->get();

    }
}
