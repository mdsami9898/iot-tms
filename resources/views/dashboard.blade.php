@extends('layouts.user-layout')

@section('content')'
<input type="hidden" name="_token" value="{{csrf_token()}}">
<div class="br-mainpanel">
{{--    <div class="pd-30">--}}
{{--        <h4 class="tx-gray-800 mg-b-5">Dashboard</h4>--}}
{{--        <h4 class="tx-gray-800 mg-b-5 demo_data">Demo Data Insert</h4>--}}

{{--    </div><!-- d-flex -->--}}
{{--    <div class="br-pagebody mg-t-5 pd-x-30">--}}
{{--        <div class="row row-sm mg-t-20">--}}
{{--            <div class="col-6">--}}
{{--                <div class="input-group m-input-group m-input-group--pill igp">--}}
{{--                    <label>Date Range</label>--}}
{{--                    <div class="form-control form-control-sm date-picker-set"--}}
{{--                         id="reportrange"--}}
{{--                    >--}}
{{--                        <i class="fa fa-calendar"--}}
{{--                           style="color:#856aff"></i>&nbsp;--}}
{{--                        <span class=""></span> <i--}}
{{--                            class="fa fa-caret-down"></i>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}
{{--    <div class="br-pagebody mg-t-5 pd-x-30">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <div class="pai_chart">--}}
{{--                    <canvas id="myChart" width="400" height="400"></canvas>--}}
{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

{{--    <div class="br-pagebody mg-t-5 pd-x-30">--}}
{{--        <div class="row">--}}
{{--            <div class="col-12">--}}
{{--                <div class="busList">--}}

{{--                </div>--}}

{{--            </div>--}}
{{--        </div>--}}
{{--    </div>--}}

    <div class="pd-30">
        <h4 class="tx-gray-800 mg-b-5">Dashboard</h4>
        <p class="mg-b-0">Do big things with Bracket, the responsive bootstrap 4 admin template.</p>
    </div>

    <div class="br-pagebody mg-t-5 pd-x-30">
        <div class="row row-sm">
            <div class="col-sm-6 col-xl-3">
                <div class="bg-teal rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Today's Revenue</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{rand(1500,40000)}} <span class="big-amount"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span></p>
                            <span class="tx-11 tx-roboto tx-white-6">24% increase expected</span>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3">
                <div class="bg-teal rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Yesterday's Revenue</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{rand(20000,40000)}} <span class="big-amount"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span></p>
                            <span class="tx-11 tx-roboto tx-white-6">26% More then today</span>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->

            <div class="col-sm-6 col-xl-3 mg-t-20 mg-xl-t-0">
                <div class="bg-primary rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <i class="fa fa-users tx-60 lh-0 tx-white op-7"></i>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Today's people count</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{rand(500,1000)}} <span class="big-amount">people</span></p>
                            <span class="tx-11 tx-roboto tx-white-6">23% average duration</span>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
            <div class="col-sm-6 col-xl-3">
                <div class="bg-teal rounded overflow-hidden">
                    <div class="pd-25 d-flex align-items-center">
                        <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg>
                        <div class="mg-l-20">
                            <p class="tx-10 tx-spacing-1 tx-mont tx-medium tx-uppercase tx-white-8 mg-b-10">Net Income</p>
                            <p class="tx-24 tx-white tx-lato tx-bold mg-b-2 lh-1">{{rand(30000,70000)}} <span class="big-amount"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span></p>
                            <span class="tx-11 tx-roboto tx-white-6">24% increase expected</span>
                        </div>
                    </div>
                </div>
            </div><!-- col-3 -->
        </div>
    </div>

    <div class="row row-sm mg-t-20">
        <div class="col-8">
            <div class="card pd-0 bd-0 shadow-base">
                <div class="card bd-0 shadow-base pd-30">
                    <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">REVENUE CHART (BAR)</h6>
                    <p class="mg-b-25">Summary of today's income of every bus.</p>


                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Ekush (22.3%)</label>
                        <div class="progress ht-5 mg-b-10">

                            <div class="progress-bar wd-25p" role="progressbar" aria-valuenow="23" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Ekattur (60.5%)</label>
                        <div class="progress ht-5 mg-b-10">
                            <div class="progress-bar bg-info wd-45p" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>

                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Shatchollish (80.5%)</label>
                        <div class="progress ht-5 mg-b-10">
                            <div class="progress-bar bg-danger wd-70p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Sholo (52.2%)</label>
                        <div class="progress ht-5 mg-b-10">
                            <div class="progress-bar bg-teal wd-60p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Chobbish (40.2%)</label>
                        <div class="progress ht-5 mg-b-10">
                            <div class="progress-bar bg-warning wd-50p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>
                    <div class="rap">
                        <span class="amount">{{rand(250,900)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                        <label class="tx-12 tx-gray-600 mg-b-10">Bahon Ponero (82.2%)</label>
                        <div class="progress ht-5 mg-b-10">
                            <div class="progress-bar bg-purple wd-65p" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    </div>





                    <div class="mg-t-20 tx-13 action">
                        <a href="" class="tx-gray-600 hover-info">Generate Report</a>
                        <div class="amount">Total: {{rand(2000,5000)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-4">
            <div class="card pd-0 bd-0 shadow-base canvas custom">

                <div class="bd pd-t-30 pd-b-20 pd-x-20 chart-custom">
                    <p class=" custom-text">Summary of today's income of every bus.</p>
                    <canvas id="chartPie" height="200"></canvas></div>
                <span class="value">
                    <span class="amount amount-1">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-2">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-3">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-4">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-5">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-6">{{rand(70,500)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                    <span class="amount amount-7">{{rand(1500,3000)}} <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 60 60" style="enable-background:new 0 0 60 60" xml:space="preserve"><path d="M10.38 9.53s2.19-1.46 4.81-.25c2.41 1.12 2.16 4.04 2.16 4.04v11.11H6.01l5.77 5.99h5.65V49.9s2.36 6.66 10.9 9.05c0 0 13.52 1.47 21.46-12.24s.98-21.69-3.27-23.53c-3.16-1.36-9.42-1.96-11.67 4.35-2.03 5.71 3.3 11.93 9.14 9.92 0 0-1.86 12.25-14.49 14.9 0 0-4.91.28-6.16-4.73v-17.2h8.98l-5.6-5.99h-3.31V11.7S24.02 1.57 15.82.91C14.51.8 11.16.49 7.33 4.53l3.05 5z" style="fill:#868e96"/></svg></span>
                </span>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-12">
            <div class="card bd-0 shadow-base pd-30 mg-t-20">
                <div class="d-flex align-items-center justify-content-between mg-b-30">
                    <div>
                        <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Detailed Bus Statistic</h6>
                        <p class="mg-b-0"><i class="icon ion-calendar mg-r-5"></i> Live Preview of every bus status</p>
                    </div>
                    <a href="{{route('live-bus-list')}}" class="btn btn-outline-info btn-oblong tx-11 tx-uppercase tx-mont tx-medium tx-spacing-1 pd-x-30 bd-2">See more</a>
                </div><!-- d-flex -->

                <table class="table table-valign-middle mg-b-0">
                    <tbody>
                    <tr>
                        <td class="pd-l-0-force">
                            Driver
                        </td>
                        <td>
                            Bus Name
                        </td>
                        <td>Location</td>
                        <td>Status</td>
                        <td>People Present</td>
                        <td>People Count</td>
                        <td>Income</td>
                    </tr>
                    <tr>
                        <td class="pd-l-0-force">
                            <img src="{{url('/images/Driver_1.png')}}" class="wd-40 rounded-circle" alt="">
                        </td>
                        <td>
                            <h6 class="tx-inverse tx-14 mg-b-0">Bahon Ekush</h6>
                            <span class="tx-12">@Delowar Mia</span>
                        </td>
                        <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                        <td><span class="red-status"></span></td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                    </tr>
                    <tr>
                        <td class="pd-l-0-force">
                            <img src="{{url('/images/Driver_2.png')}}" class="wd-40 rounded-circle" alt="">
                        </td>
                        <td>
                            <h6 class="tx-inverse tx-14 mg-b-0">Bahon Ekattur</h6>
                            <span class="tx-12">@Rahman Hossain</span>
                        </td>
                        <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                        <td><span class="red-status bg-success"></span></td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                    </tr>
                    <tr>
                        <td class="pd-l-0-force">
                            <img src="{{url('/images/Driver_3.png')}}" class="wd-40 rounded-circle" alt="">
                        </td>
                        <td>
                            <h6 class="tx-inverse tx-14 mg-b-0">Bahon Chobbish</h6>
                            <span class="tx-12">@Shahid Ullah</span>
                        </td>
                        <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                        <td><span class="red-status bg-warning"></span></td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                    </tr>
                    <tr>
                        <td class="pd-l-0-force">
                            <img src="{{url('/images/Driver_4.png')}}" class="wd-40 rounded-circle" alt="">
                        </td>
                        <td>
                            <h6 class="tx-inverse tx-14 mg-b-0">Bahon Sholo</h6>
                            <span class="tx-12">@Ramjan Ali</span>
                        </td>
                        <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                        <td><span class="red-status bg-warning"></span></td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                    </tr>
                    <tr>
                        <td class="pd-l-0-force">
                            <img src="{{url('/images/Driver_5.png')}}" class="wd-40 rounded-circle" alt="">
                        </td>
                        <td>
                            <h6 class="tx-inverse tx-14 mg-b-0">Bahon Shatchollish</h6>
                            <span class="tx-12">@Rahmot Ali</span>
                        </td>
                        <td>Mugda</td>
                        <td><span class="red-status bg-success"></span></td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                        <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                    </tr>



                    </tbody>
                </table>
            </div>
        </div>

    </div>





    {{--            FOOTER--}}
    @include('user.includes.footer')

</div>


<div id="modaldemo6" class="modal fade">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content bd-0">
            <div class="modal-header pd-y-20 pd-x-25">
                <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Bus</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body pd-25">
                @include('user.bus.form-add');
            </div>

        </div>
    </div><!-- modal-dialog -->
</div><!-- modal -->
@stop

@section('custom_script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

    <script>
        var randomScalingFactor = function() {
            return Math.round(Math.random() * 100);
        };
        var datapie = {
            datasets: [{

                data: [
                    25,
                    30,
                    29,
                    40,
                    50,
                    36,
                ],
                backgroundColor: [
                    '#00B297',
                    '#DC3545',
                    '#0866C6',
                    '#F49917',
                    '#17A2B8',
                    '#6F42c1',
                ]
            }]
        };
        var optionpie = {
            responsive: true,
            legend: {
                display: false,
            },
            animation: {
                animateScale: true,
                animateRotate: true
            }
        };

        // For a doughnut chart
        var ctx6 = document.getElementById('chartPie');
        var myPieChart6 = new Chart(ctx6, {
            type: 'doughnut',
            data: datapie,
            options: optionpie
        });
    </script>



    <script>





        var countPai =1;
        var _token = $('input[name="_token"]').val();

        let date_range = null;
        var start = moment().subtract(60, 'days');
        var end = moment().add(60, 'days');

        function default_daterange(start, end) {
            $('#reportrange span').html('');
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
            date_range = start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
            dashBoardReport(date_range);
        }

        function date_range_with_datatable_load(start, end) {
            default_daterange(start, end);

        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')]
            },
            "opens": "right"

        }, date_range_with_datatable_load);

        default_daterange(start, end);

        function dashBoardReport(date_range){
            var date_range = date_range;
            var data = {
                date_range: date_range,
                _token: _token
            }

            $.ajax({
                url:  '{!! route('users-dashboard-report') !!}' ,
                type: 'post',
                data: data,
                success: function (result) {
                    $('.appendModal').html('');
                    if (result.status == 'success') {
                        var chart = document.getElementById('myChart').getContext('2d');

                        setTimeout(() => {
                            paiChart(result.color,result.name,result.price)

                        }, 200);
                        $('.busList').html('');
                        var row = '';
                        var i = 1;
                        var result2 = result.data;
                        if(result2 === 0){
                            row += '';
                        }
                        else{

                            for(count in result2){
                                var alert = '';


                                row += '<div class="single-bub" style="background: '+result2[count].bus_color+'">' +
                                    '<span class="link" >' +
                                    '<a href="#">'+ result2[count].bus_name +'</a>' +
                                    '</span>' +
                                    '<span class="count">' +result2[count].total_price+
                                    '</span>' +
                                    '</div>';
                                i++;
                            }
                        }

                        $('.busList').append(row);

                    }
                },
                error: function (result) {

                },
                complete: function (result) {

                }
            });
        }


        var myNewChart = null;
        function paiChart(color,name,price){

            var ctx = document.getElementById('myChart').getContext('2d');
            if (countPai == 1){
                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: name,
                        datasets: [{
                            label: '# of Votes',
                            data: price,
                            backgroundColor: color,
                            borderColor: color,
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });
                myNewChart = myChart;
                countPai++;
            }else {
                myNewChart.destroy();

                var myChart = new Chart(ctx, {
                    type: 'doughnut',
                    data: {
                        labels: name,
                        datasets: [{
                            label: '# of Votes',
                            data: price,
                            backgroundColor: color,
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 2
                        }]
                    },
                    options: {
                        scales: {
                            y: {
                                beginAtZero: true
                            }
                        }
                    }
                });

                myNewChart = myChart;
            }






        }


        $(document).on('click', '.demo_data', function (e){
            e.preventDefault();
            var data = {
                _token: _token
            }
            $.ajax({
                url:  '{!! route('demo-data-insert') !!}' ,
                type: 'post',
                data: data,
                success: function (result) {
                    $('.appendModal').html('');
                    if (result.status == 'success') {
                        toastr.success(result.html, 'Success', {timeOut: 5000})



                    }
                },
                error: function (result) {

                },
                complete: function (result) {

                }
            });
        })

    </script>
@stop
