@extends('layouts.user-layout')

@section('content')'
<input type="hidden" name="_token" value="{{csrf_token()}}">
    <div class="br-mainpanel">
        <div class="pd-30">
            <h4 class="tx-gray-800 mg-b-5">Dashboard</h4>

        </div><!-- d-flex -->
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="row row-sm mg-t-20">
                <div class="col-6">
                    <input type="text" name="daterange" value="01/01/2018 - 01/15/2018" />
                </div>
            </div>
        </div>
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="row">
                <div class="col-12">
                    <div>
                        <canvas id="myChart"></canvas>
                    </div>
                </div>
            </div>
        </div>



        {{--            FOOTER--}}
        @include('user.includes.footer')

    </div>


    <div id="modaldemo6" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Bus</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    @include('user.bus.form-add');
                </div>

            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@stop

@section('custom_script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>

    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
    <script>
        var _token = $('input[name="_token"]').val();

        var start = moment().subtract(60, 'days');
        var end = moment().add(60, 'days');
        function default_daterange(start, end) {
            $('#reportrange span').html('');
            $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));

            var date_range = start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY');
            console.log(dat)
            // dashBoardReport(date_range);

        }

        $('#reportrange').daterangepicker({
            startDate: start,
            endDate: end,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')]
            },
            "opens": "right"

        }, default_daterange);
        default_daterange(start, end);

        // function dashBoardReport(date_range) {
        //     var date_range = date_range;
        //     var timezone = getLocalTimezone();
        //     var data = {
        //         date_range: date_range,
        //         timezone: timezone,
        //         _token: _token
        //     }
        //     $.ajax({
        //         url:  route('users-dashboard-report') ,
        //         type: 'post',
        //         data: data,
        //         success: function (result) {
        //             $('.appendModal').html('');
        //             if (result.status == 'success') {
        //                 $(document).find('.number_of_secret_shop').html(result.number_of_secret_shop);
        //                 $(document).find('.avg_sms').html(result.avg_sms);
        //                 $(document).find('.avg_call').html(result.avg_call);
        //                 $(document).find('.avg_voice').html(result.avg_voice);
        //                 $(document).find('.avg_touch_point').html(result.avg_touch_point);
        //                 $(document).find('.auto_responder').html(result.auto_responder);
        //                 $(document).find('.avg_email').html(result.avg_email);
        //                 var chart = document.getElementById('myPersentageChart').getContext('2d');
        //                 var sms = result.percentageOfSms;
        //                 var call = result.percentageOfCall;
        //                 var voice = result.percentageOfVoice;
        //                 var email = result.percentageOfEmails;
        //
        //                 setTimeout(() => {
        //                     if (sms == 0 && call== 0 && voice == 0){
        //                         $('.pai-chirt').addClass('no_data_found');
        //                         paiChart(40,30,30)
        //                     }else {
        //                         $('.pai-chirt').removeClass('no_data_found');
        //                         paiChart(sms,call,voice,email)
        //                     }
        //
        //                 }, 200);
        //
        //             } else if(result.status=='error') {
        //
        //             }
        //         },
        //         error: function (result) {
        //             $('#kt_modal_1_daily_usages_details').modal('show');
        //             $('.appendModal').html(result.html)
        //         },
        //         complete: function (result) {
        //
        //         }
        //     });
        // }
        //
        //
        // function paiChart(percentageOfSms,percentageOfCall,percentageOfVoice,percentageOfEmail){
        //
        //     var ctx = document.getElementById('myPersentageChart').getContext('2d');
        //
        //     var myChartFromApi = new Chart(ctx, {
        //         type: 'doughnut',
        //         data: {
        //             labels: ['SMS', 'CALL', 'VOICE','EMAIL'],
        //             datasets: [{
        //                 label: 'Percentage of sms call voice and email',
        //                 data: [percentageOfSms, percentageOfCall, percentageOfVoice,percentageOfEmail],
        //                 backgroundColor: [
        //                     'rgba(75, 192, 192, 0.8)',
        //                     'rgba(255, 99, 132, 0.8)',
        //                     'rgba(199, 25, 122, 0.8)',
        //                     'rgba(102, 25, 122, 0.8)',
        //                 ],
        //                 borderWidth: 1
        //             }]
        //         },
        //         options: {
        //             title: {
        //                 display: true,
        //                 text: 'Percentage of sms call voice and emails'
        //             }
        //         }
        //
        //     });
        // }



    </script>
@stop
