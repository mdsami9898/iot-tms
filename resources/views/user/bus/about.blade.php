@extends('layouts.user-layout')

@section('content')

@stop

@section('custom_script')
    <div class="br-mainpanel">
        <div class="pd-30">


        </div><!-- d-flex -->
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="card pd-0 bd-0 shadow-base">
                <div class="card bd-0 shadow-base pd-30">
                    <div class="row">
                        <h4 class="big-title text-center">About Us</h4>

                        <div class="content">
                            <div class="small-title">
                                Introduction:
                            </div>
                            <div class="text">
                                In this project we tried to develop a system combination of IOT, Web Application & Android Application to help manage & run a whole transport system (such as bus). We know that transport system of our country is not digitalized and not organized at all. There are no rules and regulations and an organized management does not exist. Our “IOT Based Transport Management System” will bring a whole organized system for all the management and passenger tracking, fair count, bus route management, all the profit and loss count etc. And it will be digitalized and online based.
                            </div>
                            <div class="small-title">
                                Motivation:
                            </div>
                            <div class="text">
                                The transport sector in Bangladesh is characterized by weak public and private institutions and low level of investment. Rapid population, urbanization, and motorization growth with unplanned way has been a significant cause of multidimensional problems in Dhaka city.
                                So, we decided to create a system to manage all the necessary things to run a transport system which is easy, user friendly and online based to ensure a safe and organized management system for benefiting both general people like us and also transport services.
                            </div>
                            <div class="small-title">
                                Objectives:
                            </div>
                            <div class="text">
                                <ul>
                                    <li>To create a management system to maintain the all the curriculums through online.</li>
                                    <li>To keep track of passengers coming and going. </li>
                                    <li>To count and calculate fairs of all the passengers. </li>
                                    <li>To save time. </li>
                                    <li>To save cost. </li>
                                    <li>To cut all the middlemen for better business for the owners. </li>
                                    <li>To overcome third-party harassment. </li>
                                    <li>To give better services to the passengers.</li>
                                </ul>
                            </div>
                            <div class="small-title">
                                How We Do:
                            </div>
                            <div class="text">
                                This management system will deal with all the information and fair counts of all the transports in the company. IOT based smart camera will be installed in every vehicle and a mini computer (raspberry pi) will be installed for tracing and information processing. The information will be sent through a server and company can easily monitor everything through a designated website. A website will be available to monitor and modify all the information remotely. This will save time, money and unnecessary harassment and all the data will be accurate which will ensure business quality and better service for the people. All the things about this management system are unique, combined and user-friendly.
                            </div>
                        </div>

                    </div>
                </div>
            </div>


        </div>
    </div>
@stop
