@extends('layouts.user-layout')

@section('content')

@stop

@section('custom_script')
    <div class="br-mainpanel">
    <div class="pd-30">


    </div><!-- d-flex -->
        <div class="row">
            <div class="col-12">
                <div class="card bd-0 shadow-base pd-30 mg-t-20">
                    <div class="d-flex align-items-center justify-content-between mg-b-30">
                        <div>
                            <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Detailed Bus Statistic</h6>
                            <p class="mg-b-0"><i class="icon ion-calendar mg-r-5"></i> Live Preview of every bus status</p>
                        </div>

                    </div><!-- d-flex -->

                    <table class="table table-valign-middle mg-b-0">
                        <tbody>
                        <tr>
                            <td class="pd-l-0-force">
                                Driver
                            </td>
                            <td>
                                Bus Name
                            </td>
                            <td>Location</td>
                            <td>Status</td>
                            <td>People Present</td>
                            <td>People Count</td>
                            <td>Income</td>
                        </tr>
                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_1.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Ekush</h6>
                                <span class="tx-12">@Delowar Mia</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>
                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_2.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Ekattur</h6>
                                <span class="tx-12">@Rahman Hossain</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status bg-success"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>
                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_3.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Chobbish</h6>
                                <span class="tx-12">@Shahid Ullah</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status bg-warning"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>
                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_4.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Sholo</h6>
                                <span class="tx-12">@Ramjan Ali</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status bg-warning"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>
                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_5.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Shatchollish</h6>
                                <span class="tx-12">@Rahmot Ali</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status bg-success"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>

                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_5.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">Bahon Shatchollish</h6>
                                <span class="tx-12">@Rahmot Ali</span>
                            </td>
                            <td>{{\App\Models\Bus::Location[rand(0,8)]}}</td>
                            <td><span class="red-status bg-success"></span></td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(20,45)}}</td>
                            <td class="pd-r-0-force tx-center">{{rand(500,25000)}}</td>
                        </tr>

                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_9.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0"><a href="{{url('bus-detail/'.$singleBus->id)}}">{{$singleBus->bus_name}}</a></h6>
                                <span class="tx-12">@Zawyed</span>
                            </td>
                            <td>Mugda</td>
                            <td><span class="red-status bg-success"></span></td>
                            <td class="pd-r-0-force tx-center">{{isset($rowCount) ? $rowCount :0}}</td>
                            <td class="pd-r-0-force tx-center">{{isset($singleBus->total_capacity) ? $singleBus->total_capacity : 0}}</td>
                            <td class="pd-r-0-force tx-center">{{isset($details->total_price) ? $details->total_price : 0}}</td>
                        </tr>



                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
@stop
