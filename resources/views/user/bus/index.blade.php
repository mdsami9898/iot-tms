@extends('layouts.user-layout')

@section('content')
    <div class="br-mainpanel">
        <div class="pd-30">
            <h4 class="tx-gray-800 mg-b-5">Bus Lists</h4>

        </div><!-- d-flex -->
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="row row-sm mg-t-20">
                <div class="col-2 col-offset-10">
                    <a href="#" class="btn btn-success add-bus">Add New</a>
                </div>
            </div>
        </div>
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="row">
                <div class="col-12">
                    <table id="busDataTable" class="table display responsive nowrap">
                        <thead>
                        <tr>
                            <th class="wd-15p">Bus Name</th>
                            <th class="wd-15p">Total Capacity</th>
                            <th class="wd-20p">Color</th>
{{--                            <th class="wd-20p">Action</th>--}}
                        </tr>
                        </thead>
                    </table>
                </div>
            </div>
        </div>



        {{--            FOOTER--}}
        @include('user.includes.footer')

    </div>


    <div id="modaldemo6" class="modal fade">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content bd-0">
                <div class="modal-header pd-y-20 pd-x-25">
                    <h6 class="tx-14 mg-b-0 tx-uppercase tx-inverse tx-bold">Add New Bus</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body pd-25">
                    @include('user.bus.form-add');
                </div>

            </div>
        </div><!-- modal-dialog -->
    </div><!-- modal -->
@stop

@section('custom_script')
    <script>

        $(document).on('click', '.add-bus', function (e){
            e.preventDefault();
            $('#modaldemo6').modal('show');
        })

        document.querySelectorAll('input[type=color]').forEach(function(picker) {

            var targetLabel = document.querySelector('label[for="' + picker.id + '"]'),
                codeArea = document.createElement('span');

            codeArea.innerHTML = picker.value;
            targetLabel.appendChild(codeArea);

            picker.addEventListener('change', function() {
                codeArea.innerHTML = picker.value;
                targetLabel.appendChild(codeArea);
            });
        });

        $('#bus_add_form').on('submit', function (event) {
            event.preventDefault();
            var element = $(this);

            $.ajax({
                url: '{!! route('bus-add') !!}',
                type:'post',
                data:element.serialize(),
                dataType:'json',
                success:function(result){
                    $('#preloader').hide();
                    if(result.status == 'success') {
                        busDataTable.ajax.reload();
                        $("#bus_add_form").trigger("reset");
                        $('#modaldemo6').modal('hide');
                        toastr.success(result.html, 'Success', {timeOut: 5000})


                    }
                    else if(result.status == 'error'){
                        toastr.error(result.html, 'Error', {timeOut: 5000})
                    }
                },
                error:function(result){

                },
                complete:function (result) {

                }
            });

        })

        /*DATA TABLE*/

        var _token = $('input[name="_token"]').val();
        var busDataTable = $('#busDataTable').DataTable({
            "language": {
                "search": "",
                "searchPlaceholder": "Search ",
                "lengthMenu": "_MENU_",
                "zeroRecords": "<span class=\"m-badge m-badge--default m-badge--wide m-badge--rounded\">\n" +
                    "                    <i class=\"fa fa-info-circle label-icon\"></i> No Record found." +
                    "               </span>",
                "info": "Showing page _PAGE_ of _PAGES_",
                "infoEmpty": "No records available",
                "infoFiltered": "<span class=\"m-badge m-badge--info m-badge--wide m-badge--rounded\">(filtered from _MAX_ total records)</span>",
                "paginate": {
                    "first": "",
                    "previous": '<span class="fa fa-chevron-left"></span>',
                    "next": '<span class="fa fa-chevron-right"></span>',
                    "last": ""
                },
            },
            "pagingType": "full_numbers",
            "order": [[ 1, "desc" ]],
            "lengthMenu": [[5, 10, 25, 50, -1], [5, 10, 25, 50, "All"]],
            pageLength: 10,
            processing: true,
            serverSide: true,
            responsive: true,
            stateSave: true,
            "renderer": "bootstrap",
            ajax:  {
                url: '{!! route('bus-data-table') !!}',
            },
            columns: [
                // {data: 'profile_image', name: 'profile_image', "orderable": false, "searchable":false,width:"10%"},

                {data: 'bus_name', name: 'bus_name', "orderable": true, "searchable":true,width:"15%"},
                {data: 'total_capacity', name: 'total_capacity', "orderable": true, "searchable":false,width:"15%"},
                {data: 'color', name: 'color', "orderable": true, "searchable":false,width:"15%"},
                // {data: 'action', name: 'action', "orderable": true, "searchable":true,width:"15%"},
            ],
            columnDefs: [
                // { responsivePriority: 1, targets: 0, className: 'noVis' },
                // { responsivePriority: 2, targets: 5, className: 'noVis' },
                // { responsivePriority: 3, targets: 1, className: 'noVis' },
                // { responsivePriority: 4, targets: -1 },
            ],
            dom: '<"top-toolbar"<"top-left-toolbar"lB><"top-right-toolbar"f>>rt<"bottom-toolbar"<"bottom-left-toolbar"i><"bottom-right-toolbar"p>>',
            buttons:[

                // {
                //     extend: 'colvisGroup',
                //     text: 'Office info',
                //     show: [ 1, 2 ],
                //     hide: [ 3, 4, 5 ]
                // },
                // {
                //     extend: 'colvisGroup',
                //     text: 'HR info',
                //     show: [ 3, 4, 5 ],
                //     hide: [ 1, 2 ]
                // },
                // {
                //     extend: 'colvisGroup',
                //     text: 'Show all',
                //     show: ':hidden'
                // }
            ],
            "drawCallback": function(settings) {
                // $("#user-datatable_paginate").html("aflk;klas");
                // alert($(".custom-select").val());
            },
            "fnPreDrawCallback": function( oSettings ) {
            }
        });


    </script>
@stop
