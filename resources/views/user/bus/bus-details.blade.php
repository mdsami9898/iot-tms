@extends('layouts.user-layout')

@section('content')
    <div class="br-mainpanel">
        <div class="pd-30">


        </div><!-- d-flex -->
        <div class="row">
            <div class="col-12">
                <div class="card bd-0 shadow-base pd-30 mg-t-20">
                    <div class="d-flex align-items-center justify-content-between mg-b-30">
                        <div>
                            <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Bus Name: {{$singleBus->bus_name}}</h6>
                            <p class="mg-b-0"><i class="icon ion-calendar mg-r-5"></i> Live Preview of every bus status</p>
                        </div>

                    </div><!-- d-flex -->

                    <table class="table table-valign-middle mg-b-0">
                        <tbody>
                        <tr>
                            <td class="pd-l-0-force">
                                Driver
                            </td>
                            <td>
                                Bus Name
                            </td>
                            <td>Location</td>
                            <td>Status</td>
                            <td>People Present</td>
                            <td>People Count</td>
                            <td>Income</td>
                        </tr>

                        <tr>
                            <td class="pd-l-0-force">
                                <img src="{{url('/images/Driver_9.png')}}" class="wd-40 rounded-circle" alt="">
                            </td>
                            <td>
                                <h6 class="tx-inverse tx-14 mg-b-0">{{$singleBus->bus_name}}</h6>
                                <span class="tx-12">@Zawyed</span>
                            </td>
                            <td>Mugda</td>
                            <td><span class="red-status bg-success"></span></td>
                            <td id="rowCount" class="pd-r-0-force tx-center">{{isset($rowCount) ? $rowCount : 0}}</td>
                            <td class="pd-r-0-force tx-center">{{isset($singleBus->total_capacity) ? $singleBus->total_capacity :0}}</td>
                            <td class="pd-r-0-force tx-center">{{isset($details->total_price) ? $details->total_price : 0}}</td>
                        </tr>



                        </tbody>
                    </table>
                </div>
            </div>

        </div>
        <div class="row">
            <div class="col-12">
                <div class="card bd-0 shadow-base pd-30 mg-t-20">
                    <div class="location-area">
                        <span class="line">

                            <span id="location-tag" class="location-tag" style="left: 100px"></span>

                            <span class="location location1"></span>
                            <span class="location_name location_name1">Khilgao</span>
                            <span class="location location2"></span>
                            <span class="location_name location_name2">Mugda</span>
                            <span class="location location3"></span>
                            <span class="location_name location_name3">Komolapur</span>
                            <span class="location location4"></span>
                            <span class="location_name location_name4">Motijhil</span>
                            <span class="location location5"></span>
                            <span class="location_name location_name5">Polton</span>
                            <span class="location location6"></span>
                            <span class="location_name location_name6">Shahbag</span>
                            <span class="location location7"></span>
                            <span class="location_name location_name7">Kolabagan</span>
                            <span class="location location8"></span>
                            <span class="location_name location_name8">Dhanmondi 27</span>
                            <span class="location location9"></span>
                            <span class="location_name location_name9">Asad Gate</span>
                            <span class="location location10"></span>
                            <span class="location_name location_name10">Mirpur 1</span>

                        </span>
                    </div>
                </div>
            </div>

        </div>
    </div>
@stop

@section('custom_script')

    <script type="text/javascript" src="https://cdn.jsdelivr.net/jquery/latest/jquery.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
    <script type="text/javascript" src="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.min.js"></script>
    <script>



        var position = 100;
        function ff(){
            position = position+5;
            document.getElementById("location-tag").style.left = +position+"px";
            apiCall();
            }
        function apiCall(){
            $.ajax({
                type: "get",
                url: '/get-dynamic-data',
                data:{},
                dataType:'json',
                success:function(result){
                    console.log(result.html);
                    $('#rowCount').html(result.html);

                },
                error:function(result){

                },
                complete:function (result) {

                }
            });
        }

        setInterval(ff, 5000);
    </script>
@stop
