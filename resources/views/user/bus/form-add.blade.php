<div class="form-layout form-layout-1">
    <form id="bus_add_form">
        <input type="hidden" name="_token" value="{{csrf_token()}}">
        <div class="row mg-b-25">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Bus Name: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="bus_name" placeholder="Enter Bus Name">
                </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Total Capacity: <span class="tx-danger">*</span></label>
                    <input class="form-control" type="text" name="total_capacity" placeholder="Enter Total capacity">
                </div>
            </div><!-- col-4 -->
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-control-label">Select Bus Color: </label>
                    <span class="color-picker">
                  <label for="colorPicker">
                    <input type="color" name="bus_color" id="colorPicker" value="">
                  </label>
                </span>
                </div>
            </div><!-- col-4 -->

        </div><!-- row -->

        <div class="form-layout-footer">
            <button class="btn btn-info">Submit</button>
        </div><!-- form-layout-footer -->
    </form>
</div>
