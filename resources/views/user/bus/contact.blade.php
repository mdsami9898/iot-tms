@extends('layouts.user-layout')

@section('content')

@stop

@section('custom_script')
    <div class="br-mainpanel">
        <div class="pd-30">


        </div><!-- d-flex -->
        <div class="br-pagebody mg-t-5 pd-x-30">
            <div class="row">
                <div class="col-6">
                    <div class="card pd-0 bd-0 shadow-base">
                        <div class="card bd-0 shadow-base pd-30">
                            <div class="contact-info">
                                <div class="img">
                                    <img src="{{url('/images/z.jpg')}}" alt="">
                                </div>

                                <div class="info">
                                    <ul>
                                        <li class="name">Zawyed Ivna Zion</li>
                                        <li>Transporter Management Leader</li>
                                        <li>Engineer in Computer Science</li>
                                        <li>Phone: 01701446616</li>
                                    </ul>
                                </div>
                                <div class="icon">

                                    <a href="https://www.facebook.com/iZawyed"><i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="https://www.gmail.com"><i class="fa fa-envelope"></i>
                                    </a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-6">
                    <div class="card pd-0 bd-0 shadow-base">
                        <div class="card bd-0 shadow-base pd-30">
                            <div class="contact-info">
                                <div class="img">
                                    <img src="{{url('/images/s.jpg')}}" alt="">
                                </div>

                                <div class="info">
                                    <ul>
                                        <li class="name">Shaman Shah</li>
                                        <li>Managing Director</li>
                                        <li>Engineer in Computer Science</li>
                                        <li>Phone : 01915605658</li>
                                    </ul>
                                </div>
                                <div class="icon">

                                    <a href="https://www.facebook.com/profile.php?id=100008285390889"><i class="fa fa-facebook"></i>
                                    </a>
                                    <a href="https://www.gmail.com"><i class="fa fa-envelope"></i>
                                    </a>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <div class="row">
                <div class="col-12 address">
                    <div class="card pd-0 bd-0 shadow-base">
                        <div class="card bd-0 shadow-base pd-30">
                            <div class="contact-info">
                                <h4></h4>
                                <div class="icon">
                                    <h4><i class="fa fa-home"></i> Contact Us</h4>
                                </div>
                                <div class="text">
                                    Daffodil International University<br>102/1, Sukrabad, Mirpur Rd, Dhaka
                                    <br>busmanager69@gmail.com

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>



        </div>
    </div>
@stop
