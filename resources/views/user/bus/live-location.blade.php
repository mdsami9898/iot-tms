@extends('layouts.user-layout')

@section('content')

@stop

@section('custom_script')
    <div class="br-mainpanel">
        <div class="pd-30">
            <div class="d-flex align-items-center justify-content-between mg-b-30">
                <div>
                    <h6 class="tx-13 tx-uppercase tx-inverse tx-semibold tx-spacing-1">Live Location</h6>
                    <p class="mg-b-0"><i class="icon ion-calendar mg-r-5"></i> Curent location of every bus on google map and path</p>
                </div>

            </div><!-- d-flex -->

        </div><!-- d-flex -->
        <div class="row">

            <div class="col-6">
                <div class="map-image">
                    <img src="{{url('/images/map1.jpg')}}" alt="">
                </div>
            </div>
            <div class="col-6">
                <div class="map-image">
                    <img src="{{url('/images/'.rand(1,4).'.png')}}" alt="">
                </div>
            </div>

        </div>
    </div>
@stop
