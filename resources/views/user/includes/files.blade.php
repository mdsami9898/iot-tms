<!DOCTYPE html>
<html lang="en">

<!-- Mirrored from themetrace.com/templates/bracket/app/ by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 27 Jun 2019 17:50:14 GMT -->
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">


    <!-- Twitter -->
    <meta name="twitter:site" content="@themepixels">
    <meta name="twitter:creator" content="@themepixels">
    <meta name="twitter:card" content="summary_large_image">
    <meta name="twitter:title" content="Bracket">
    <meta name="twitter:description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="twitter:image" content="{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/themepixels.me/bracket/img/bracket-social.html">

    <!-- Facebook -->
    <meta property="og:url" content="http://themepixels.me/bracket">
    <meta property="og:title" content="Bracket">
    <meta property="og:description" content="Premium Quality and Responsive UI for Dashboard.">

    <meta property="og:image" content="{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/themepixels.me/bracket/img/bracket-social.html">
    <meta property="og:image:secure_url" content="{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/{{asset('admin/')}}/themepixels.me/bracket/img/bracket-social.html">
    <meta property="og:image:type" content="image/png">
    <meta property="og:image:width" content="1200">
    <meta property="og:image:height" content="600">

    <!-- Meta -->
    <meta name="description" content="Premium Quality and Responsive UI for Dashboard.">
    <meta name="author" content="ThemePixels">

    <title>Iot TMS</title>

    <!-- vendor css -->
    <link href="{{asset('admin/')}}/lib/font-awesome/css/font-awesome.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/Ionicons/css/ionicons.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/jquery-switchbutton/jquery.switchButton.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/rickshaw/rickshaw.min.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/chartist/chartist.css" rel="stylesheet">

    <link href="{{asset('admin/')}}/lib/highlightjs/github.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/datatables/jquery.dataTables.css" rel="stylesheet">
    <link href="{{asset('admin/')}}/lib/select2/css/select2.min.css" rel="stylesheet">

    <!-- Bracket CSS -->



    <link rel="stylesheet" href="{{asset('admin/')}}/css/datatable.css">
    <link rel="stylesheet" href="{{asset('admin/')}}/css/toster.min.css">
    <link rel="stylesheet" href="{{asset('admin/')}}/css/bracket.css">
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
    <style>
        .pai_chart {
            width: 35%;
            margin: 45px auto;
        }
        .single-bub {
            padding: 8px;
            margin: 15px 0;
        }

        .single-bub .count {
            margin-left: 177px;
            color: #000;
            font-weight: 900;
        }
        h4.tx-gray-800.mg-b-5.demo_data {
            font-size: 12px;
            background-color: #ddd;
            display: inline-block;
            padding: 10px;
            cursor: pointer;
        }
        span.red-status {
            width: 12px;
            height: 20px;
            background-color: red;
            z-index: 999;
            display: inline-block;
        }

        .pd-25.d-flex.align-items-center svg {
            width: 50px;
            height: auto;
        }
        .tx-10 {
            font-size: 8px;
        }
        i.fa.fa-users.tx-60.lh-0.tx-white.op-7 {
            font-size: 50px;
        }

        .rap {
            position: relative;
        }

        span.amount {
            position: absolute;
            right: 19px;
            top: 10px;
        }
        .mg-t-20.tx-13.action {
            position: relative;
        }

        .amount {
            position: absolute;
            right: 0;
            top: 5px;
        }
        .amount svg {
            width: 10px;
        }


        /*cHART CSS*/
        .chart-custom {
            position: relative;
        }

        span.amount.amount-1 {
            left: 44px;
            top: 80px;
        }

        span.amount.amount-2 {
            right: 88px;
            top: 65px;
        }

        span.amount.amount-3 {
            right: 9px;
            top: 127px;
        }

        span.amount.amount-4 {
            right: 9px;
            top: 319px;
        }

        span.amount.amount-5 {
            top: 367px;
            left: 154px;
        }

        span.amount.amount-6 {
            top: 327px;
            left: 21px;
        }
        span.amount.amount-7 {
            left: 130px;
            top: 198px;
        }
        .card.pd-0.bd-0.shadow-base.canvas.custom {
            height: 437px;
        }
        span.amount.amount-7 {
            font-size: 35px;
        }
        /*cHART CSS*/

        span.big-amount {
            display: inline-block;
            font-size: 11px;
            margin-left: 5px;
        }

        span.big-amount svg {
            width: 16px !important;
        }
        .map-image {
            padding: 4px;
            display: inline-block;
        }

        .map-image img {
            width: 100%;
        }


        .small-title {
            margin: 20px 1px;
            font-size: 18px;
            color: #5d5d5d;
            font-weight: 900;
        }

        .icon svg {
            width: 30px;
            display: inline-block;
            margin: 15px;
        }
        .info ul {
            padding: 0;
            margin: 0;
            list-style: none;
            font-size: 14px;
        }
        li.name {
            font-size: 28px;
            font-weight: 900;
        }
        .icon i {
            font-size: 25px;
            display: inline-block;
            color: #000;
            margin-right: 14px;
            margin-top: 10px;
        }
        .img {
            width: 100%;
        }

        .img img {
            width: 100%;
        }
        .col-12.address {
            margin-top: 20px;
        }



        span.line {
            background-color: red;
            width: 100%;
            height: 2px;
            display: inline-block;
            z-index: 9999 !important;
            color: red;
            position: relative;
        }

        span.location {
            position: absolute;
            width: 10px;
            height: 10px;
            background-color: green;
            left: 0;
            top: -4px;
        }
        span.location_name {
            position: absolute;
            width: 100px;
            height: 10px;
            color:#000;
            left: 0;
            top: 16px;
        }
        span.location.location2 {
            left: 100px;
        }
        span.location_name.location_name2 {
            left: 100px;
        }


        span.location.location3 {
            left: 200px;
        }
        span.location_name.location_name3 {
            left: 200px;
        }

        span.location.location4 {
            left: 300px;
        }
        span.location_name.location_name4 {
            left: 300px;
        }

        span.location.location5 {
            left: 400px;
        }
        span.location_name.location_name5 {
            left: 400px;
        }

        span.location.location6 {
            left: 500px;
        }
        span.location_name.location_name6 {
            left: 500px;
        }
        span.location.location7 {
            left: 600px;
        }
        span.location_name.location_name7 {
            left: 600px;
        }
        span.location.location8 {
            left: 700px;
        }
        span.location_name.location_name8 {
            left: 700px;
        }
        span.location.location9 {
            left: 800px;
        }
        span.location_name.location_name9 {
            left: 800px;
        }
        span.location.location10 {
            left: 900px;
        }
        span.location_name.location_name10 {
            left: 900px;
        }

        span.location-tag {
            width: 20px;
            height: 20px;
            position: absolute;
            left: 5px;
            top: -29px;
            background-color: #000;
            border-radius: 50%;
        }


    </style>
</head>
