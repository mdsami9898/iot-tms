<div class="br-logo"><a href=""><span>[</span>IOT-TMS<span>]</span></a></div>
<div class="br-sideleft overflow-y-auto">
    <div class="br-sideleft-menu">
        {{--@if( request()->url() == route('admin.dashboard')) {{'active'}} @endif--}}
        <a href="{{route('dashboard')}}" class="br-menu-link @if( request()->url() == route('dashboard')) {{'active'}} @endif">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Dashboard</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

{{--        <a href="{{route('bus-list')}}" class="br-menu-link @if( request()->url() == route('bus-list')) {{'active'}} @endif">--}}
{{--            <div class="br-menu-item">--}}
{{--                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>--}}
{{--                <span class="menu-item-label">Bus List</span>--}}
{{--            </div><!-- menu-item -->--}}
{{--        </a><!-- br-menu-link -->--}}

        <a href="{{route('live-bus-list')}}" class="br-menu-link @if( request()->url() == route('live-bus-list')) {{'active'}} @endif">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-ios-home-outline tx-22"></i>
                <span class="menu-item-label">Live Bus List</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

        <a href="{{route('live-location')}}" class="br-menu-link @if( request()->url() == route('live-location')) {{'active'}} @endif">
            <div class="br-menu-item">
                <i class="menu-item-icon icon icon ion-navigate tx-22"></i>
                <span class="menu-item-label">Live Location</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

        <a href="{{route('about')}}" class="br-menu-link @if( request()->url() == route('about')) {{'active'}} @endif">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-map tx-22"></i>
                <span class="menu-item-label">About</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->

        <a href="{{route('contact')}}" class="br-menu-link @if( request()->url() == route('contact')) {{'active'}} @endif">
            <div class="br-menu-item">
                <i class="menu-item-icon icon ion-information-circled tx-22"></i>
                <span class="menu-item-label">Contact</span>
            </div><!-- menu-item -->
        </a><!-- br-menu-link -->
    </div><!-- br-sideleft-menu -->





    <br>
</div>
