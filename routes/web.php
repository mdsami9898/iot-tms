<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect()->route('login');
});

Route::get('/dashboard', function () {
    return view('dashboard');
})->middleware(['auth'])->name('dashboard');

require __DIR__.'/auth.php';

Route::group(['middleware'=> 'auth'], function () {

    Route::get('/bus-list', [\App\Http\Controllers\BusController::class, 'index'])->name('bus-list');
    Route::post('/bus-add', [\App\Http\Controllers\BusController::class, 'insertBus'])->name('bus-add');
    Route::get('/bus-data-table', [\App\Http\Controllers\BusController::class, 'busDataTable'])->name('bus-data-table');
    Route::post('/users-dashboard-report', [\App\Http\Controllers\BusController::class, 'dashboardReport'])->name('users-dashboard-report');
    Route::post('/demo-data-insert', [\App\Http\Controllers\BusController::class, 'demoDataInsert'])->name('demo-data-insert');
    Route::get('/live-bus-list', [\App\Http\Controllers\BusController::class, 'liveBus'])->name('live-bus-list');
    Route::get('/live-location', [\App\Http\Controllers\BusController::class, 'liveLocation'])->name('live-location');
    Route::get('/about', [\App\Http\Controllers\BusController::class, 'about'])->name('about');
    Route::get('/contact', [\App\Http\Controllers\BusController::class, 'contact'])->name('contact');
    Route::get('/bus-detail/{id}', [\App\Http\Controllers\BusController::class, 'busDetail'])->name('bus-detail');
    Route::get('/get-dynamic-data', [\App\Http\Controllers\BusController::class, 'busDynamicData'])->name('get-dynamic-data');


});
